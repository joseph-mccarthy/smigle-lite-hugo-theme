# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.0] - 2024-04-01

### Changed

- Updated readme badges for new version and updated changelog.
- [#10](https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/issues/10) updated the example site to show how to use sections and demo recursion.

### Fixed

- Fixed the spacing between footnotes to match rest of the theme and not overlap on mobile.
- [#11](https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/issues/11) Updated the latest posts title to be H3 rather than H1.
- [#7](https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/issues/7) updated the site header to no longer be a H1 tag to help with SEO. Style remains the same as H1.

### Added

- Post listing is now recursive for better organisation of content.
- [#6](https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/issues/6) Post navigation at the bottom of the current post to move to the next and previous post using post title.
- [#8](https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/issues/8) Added meta tags for pages, default from config, added examples of this in example-site with details for adding data to tags and categories.
- Updated category and tag post pages to allow a description to be added to the top of the listed posts. Updated the example site to show examples of this.

## [1.1.0] - 2024-03-17

### Added

- This CHANGELOG file
- Added badges to the README
- [#5](https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/issues/5) Adding an example-site show that the theme can be tested before applying to site.
- [#4](https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/issues/4) Adding styling for footnotes within the text to add brackets around the number as well as remove the text decoration. 
- [#3](https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/issues/3) Added configuration for site content license information in the footer. If none provided then no license information is outputted. License link now opens in new tab.

### Removed

- removed social example reference to codeberg from the example-config.yaml
- [#5](https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/issues/5) Removed the example-config.yaml from the root, as there is now an example-site

### Changed

- [#1](https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/issues/1) Updated Readme on how to configure the latest post section
- Social links in the footer now open in a new tab.
- [#3](https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/issues/3) Updated Readme to add information on license configuration. 

### Fixed

- Fixed some typos in the README
- [#1](https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/issues/1) Default to not enabled, and if enabled without a post count, then default post count is set to three.
- [#2](https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/issues/2) Fixed latest posts to not include the currently viewed post.

## [1.0.0] - 2024-03-08

### Added

- Last 5 published posts at the bottom of content.

### Changed

- Footer to state theme being used and links back to smigle and smol
- Spacing on elements after removal of elements

### Removed

- Removed the need for a user profile image.
- Removed the need for a site tag line.
- Removed the time to read post.
- Removed the word count on posts.
- Removed styling on taxonomies pages

[unreleased]: https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/compare/1.2.0...main
[1.2.0]: https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/compare/1.1.0...1.2.0
[1.1.0]: https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/compare/1.0.0...1.1.0
[1.0.0]: https://codeberg.org/joseph-mccarthy/smigle-lite-hugo-theme/releases/tag/1.0.0
